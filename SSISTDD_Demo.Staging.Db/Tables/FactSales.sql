﻿CREATE TABLE [dbo].[FactSalesPerformance]
(
	[UserNK] VARCHAR(50) NOT NULL, 
    [AccountNK] VARCHAR(50) NOT NULL, 
    [SalesAmount] DECIMAL(18, 2) NULL, 
    [TargetAmount] DECIMAL(18, 2) NOT NULL, 
    [Performance] DECIMAL(18, 2) NOT NULL, 
    CONSTRAINT [PK_FactSales] PRIMARY KEY ([UserNK], [AccountNK])
)
