﻿using NUnit.Framework;
using SSISTDD;
using ExtractDb = SSISTDD_Demo.Extract.DataModel;
using StagingDb = SSISTDD_Demo.Staging.DataModel;

namespace SSISTDD_Demo.Staging.UnitTests
{
    [TestFixture]
    public class FactSalesPerformance
    {
        DbController extractDc;
        DbController stagingDc;
        EntityFactory<ExtractDb.Sale> sales;
        EntityFactory<ExtractDb.Target> targets;
        EntityFactory<StagingDb.FactSalesPerformance> factSalesPerformance;
        [TestFixtureSetUp]
        public void Setup()
        {
            //Get Database Controllers
            extractDc = DbControllerFactory.NewDbController(SourceConfigs.UK, DbConfigs.Extract);
            stagingDc = DbControllerFactory.NewDbController(SourceConfigs.UK, DbConfigs.Staging);

            //Get Entity Factories
            sales = extractDc.GetEntityFactory<ExtractDb.Sale>();
            targets = extractDc.GetEntityFactory<ExtractDb.Target>();
            factSalesPerformance = stagingDc.GetEntityFactory<StagingDb.FactSalesPerformance>();

            //Clear down both source and destination tables related to Entiy factories above
            extractDc.ClearDown(false, true);
            stagingDc.ClearDown(false, true);

            //Do setup of source data
            SalesAreInsertedIntoFactSalesPerformance_Setup();

            //insert  all new Entities related to Entiy factories above
            extractDc.Insert();

            //Run SSIS Package
            new SSISPackage(SourceConfigs.UK, SsisConfigs.ExtractConform, "ExtractConform.Sales.dtsx").RunPackage();
        }

        [Test]
        public void SalesAreInsertedIntoFactSalesPerformance()
        {
            var sale = factSalesPerformance.Where(s => s.UserNK == "R1");
            Assert.IsNotNull(sale);
            Assert.AreEqual("Acc_1", sale.AccountNK);
            Assert.AreEqual(396.11m, sale.SalesAmount);
            Assert.AreEqual(200.00m, sale.TargetAmount);
            Assert.AreEqual(decimal.Round((200.00m / 396.11m), 2), sale.Performance);
        }

        public void SalesAreInsertedIntoFactSalesPerformance_Setup()
        {
            var sale = sales.NewEntity(false, null);
            sale.User = "R1";
            sale.Account = "Acc_1";
            sale.Amount = "£396.11";

            var target = targets.NewEntity(false, null);
            target.User = "R1";
            target.Amount = "200";
        }
    }
}
