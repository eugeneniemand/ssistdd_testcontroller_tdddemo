﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSISTDD_Demo.Staging.UnitTests
{
    public sealed class DbConfigs : SSISTDD.DbConfigsEnum
    {
        public static readonly DbConfigs Extract = new DbConfigs("Extract");
        public static readonly DbConfigs Staging = new DbConfigs("Staging");

        public DbConfigs(string value) : base(value)
        {
        }
    }

    public sealed class SourceConfigs : SSISTDD.SourceConfigsEnum
    {
        public static readonly SourceConfigs UK = new SourceConfigs("UK");        

        public SourceConfigs(string value)
            : base(value)
        {
        }
    }

    public sealed class SsisConfigs : SSISTDD.SsisConfigsEnum
    {
        public static readonly SsisConfigs ExtractConform = new SsisConfigs("ExtractConform");
        public static readonly SsisConfigs TransformPublish = new SsisConfigs("TransformPublish");

        public SsisConfigs(string value)
            : base(value)
        {
        }
    }
}
