﻿-- SETUP DEMO DATA

insert into 
	Sales
values 
	('R1','A','£396.22'),
	('R2','B','£221.57'),
	('R3','C','£315.82')

insert into 
	Targets
values
	('R1', '£400'),
	('R2', '£500'),
	('R3', '£290'),
	('M1', '£800')

insert into 
	Teams
Values
	('M!','R1'),
	('M!','R2'),
	('M!','R3')