﻿CREATE TABLE [dbo].[Sales]
(
	[User] VARCHAR(10) NOT NULL,
	[Account] VARCHAR(10) NOT NULL, 
    [Amount] VARCHAR(10) NULL, 
    CONSTRAINT [PK_Sales] PRIMARY KEY ([User], [Account])
)
