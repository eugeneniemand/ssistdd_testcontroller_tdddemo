﻿CREATE TABLE [dbo].[Teams]
(
	[Manager] varchar(10) NOT NULL,
	[Rep] varchar(10) NOT NULL, 
    CONSTRAINT [PK_DimTeams] PRIMARY KEY ([Manager], [Rep]) 
)
