﻿CREATE PROCEDURE [dbo].[ExtractSalesPerformance]
	
AS
	With CleanedData as (
		SELECT 
			[s].[User] as UserNK, 
			[s].[Account] as AccountNK, 
			cast(replace([s].[Amount],'£','') as decimal(18,2)) as SalesAmount, 
			cast(replace([t].[Amount],'£','') as decimal(18,2)) as TargetAmount
		from
			dbo.Sales as s 
			inner join dbo.Targets as t 
			on s.[User] = t.[User]
		)
	select 
		[UserNK], 
		[AccountNK], 
		[SalesAmount], 
		[TargetAmount],
		cast(round([TargetAmount] / [SalesAmount],2) as decimal(18,2)) as Performance
	from 
		CleanedData
