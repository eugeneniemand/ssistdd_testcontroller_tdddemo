﻿CREATE TABLE [dbo].[DimCommissionsBand]
(
	[CommissionsBandSK] INT NOT NULL PRIMARY KEY,
	LowerBound INT NOT NULL,
	UpperBound INT NOT NULL,
	Rate Decimal NOT NULL,
	CommissionsBandName varchar(50) NOT NULL
)
