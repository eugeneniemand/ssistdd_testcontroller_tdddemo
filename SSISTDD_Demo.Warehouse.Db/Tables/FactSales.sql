﻿CREATE TABLE [dbo].[FactSalesPerformance]
(
	FactSalesPerformanceSK INT NOT NULL,
	[UserSK] VARCHAR(50) NOT NULL, 
    [AccountSK] VARCHAR(50) NOT NULL, 
    [SalesAmount] DECIMAL(18, 2) NOT NULL, 
    [TargetAmount] DECIMAL(18, 2) NOT NULL, 
    [Performance] DECIMAL(18, 2) NOT NULL, 
    CONSTRAINT [PK_FactSales] PRIMARY KEY ([FactSalesPerformanceSK])
)
